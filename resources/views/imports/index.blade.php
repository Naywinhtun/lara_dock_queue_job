<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Import CSV</title>
</head>
<body>
    <h3>CSV Import</h3>
    <form action="{{ route('import') }}" method="post" enctype="multipart/form-data">
        @csrf    
        <input type="file" name="file">
        <br>
        <button class="btn btn-primary" type="submit">upload</button>
    </form>
</body>
</html>