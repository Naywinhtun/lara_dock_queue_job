<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ImportCsvJob;
use Illuminate\Support\Facades\Storage;

class CsvImportController extends Controller
{
    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:csv,txt',
        ]);

        $path = $request->file('file')->store('csv_uploads');

        ImportCsvJob::dispatch($path);

        return response()->json(['message' => 'CSV file uploaded and import job dispatched successfully.']);
    }
}
